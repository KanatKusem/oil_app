Rails.application.routes.draw do
  resources :energy_usages
  resources :productions
  resources :users
  resources :reserves
  resources :live_data
  resources :energy_usages
  
 root 'static_pages#home'
  get '/contact', to: 'static_pages#contact'
  get '/about', to: 'static_pages#about'
  get '/signup', to: 'users#new'
  post '/signup', to: 'users#create'
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  get 'productions/show', to: 'productions#show'
  post 'productions/show', to: 'productions#show'
  get 'energy_usages', to: 'energy_usages#index'

  resources :users
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  
  
  
  end
