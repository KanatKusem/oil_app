require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test "full title helper" do
    assert_equal full_title, "Oil statistics App"
    assert_equal full_title("Home"), "Home | Oil statistics App"
  end
end