json.extract! energy_usage, :id, :country, :energy_use, :gdp_per_capita, :created_at, :updated_at
json.url energy_usage_url(energy_usage, format: :json)
