class ReservesController < ApplicationController
   before_action :logged_in_user, only: [:index, :edit, :update, :destroy]
  before_action :correct_user, only: [:edit, :update]
  before_action :admin_user, only: :destroy
  
  # Displays results of matches played thus far
  def index
    if !logged_in?
      flash[:warning] = "Please log in to access results."
      redirect_to login_path
    else
       
    end
  end


 
end
