class EnergyUsagesController < ApplicationController
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy]
  before_action :correct_user, only: [:edit, :update]
  before_action :admin_user, only: :destroy
  
  # Displays results of matches played thus far
  def index
    if !logged_in?
      flash[:warning] = "Please log in to access results."
      redirect_to energy_usage_path
    else
       @energy_usages = EnergyUsage.all
    end
  end

 

  # GET /energy_usages/1
  # GET /energy_usages/1.json
  def show
  end

  # GET /energy_usages/new
  def new
    @energy_usage = EnergyUsage.new
  end

  # GET /energy_usages/1/edit
  def edit
  end

  # POST /energy_usages
  # POST /energy_usages.json
  def create
    @energy_usage = EnergyUsage.new(energy_usage_params)

    respond_to do |format|
      if @energy_usage.save
        format.html { redirect_to @energy_usage, notice: 'Energy usage was successfully created.' }
        format.json { render :show, status: :created, location: @energy_usage }
      else
        format.html { render :new }
        format.json { render json: @energy_usage.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /energy_usages/1
  # PATCH/PUT /energy_usages/1.json
  def update
    respond_to do |format|
      if @energy_usage.update(energy_usage_params)
        format.html { redirect_to @energy_usage, notice: 'Energy usage was successfully updated.' }
        format.json { render :show, status: :ok, location: @energy_usage }
      else
        format.html { render :edit }
        format.json { render json: @energy_usage.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /energy_usages/1
  # DELETE /energy_usages/1.json
  def destroy
    @energy_usage.destroy
    respond_to do |format|
      format.html { redirect_to energy_usages_url, notice: 'Energy usage was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_energy_usage
      @energy_usage = EnergyUsage.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def energy_usage_params
      params.require(:energy_usage).permit(:country, :energy_use, :gdp_per_capita)
    end
end
