class ApplicationMailer < ActionMailer::Base
  default from: 'oil_stat@application.com'
  layout 'mailer'
end
