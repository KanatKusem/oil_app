# needs gem 'rake' to be insstalled
# needs Result model to be created and db:migrate executed
# to run task in command line write: rake results:parse_data

require 'csv'
require 'open-uri'

 

namespace :oil_stats do
  desc "Parsing CSV file from website and store it to DB"
  task parse_data: [:environment] do
    ActiveRecord::Base.connection.execute("TRUNCATE TABLE productions")
    #csv_text = open('https://github.com/KanatKusem/oil_production_data/blob/master/UNdata_Export_20171114_174427137.csv')
    csv_text = File.read(Rails.root.join('lib', 'seeds', 'UNdata_Export_20171114_174427137.csv'))

    csv = CSV.parse(csv_text, :headers => true, quote_char: "\x00")
    csv.each do |row|
      #if (row['Year'] == "2012, 2013, 2014" )
      Production.create!(:country => row['Country or Area'], :year => row['Year'], :quantity => row['Quantity'])
      end
    
      puts "#{Time.now} - Success!"
     end
  end


namespace :oil_stats do
  desc "Parsing CSV file from website and store it to DB"
  task parse_reserves: [:environment] do
    ActiveRecord::Base.connection.execute("TRUNCATE TABLE reserves")
    csv_text = File.read(Rails.root.join('lib', 'seeds', 'provedoilreserves.csv'))


    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      
      Reserve.create!(:country => row['Country'], :reservation => row['Reserves Amount'])
      
    end
      puts "#{Time.now} - Success!"
    end
  end

namespace :oil_stats do
  desc "Parsing CSV file from website and store it to DB"
  task test: [:environment] do
      
    @test = EnergyUsage.select('distinct country, energy_use, gdp_per_capita, population').where.not(energy_use: nil, gdp_per_capita: nil, population: nil).map{|f| [f.energy_use, f.gdp_per_capita, f.population, f.country]}

     # puts "#{Time.now} - Success!"
      puts "#{@test}"

    end
  end


namespace :oil_stats do
  desc "Parsing CSV file from website and store it to DB"
  task usage: [:environment] do
        ActiveRecord::Base.connection.execute("TRUNCATE TABLE energy_usages")
    csv_text = File.read(Rails.root.join('lib', 'seeds', 'energy-use-per-capita-vs-gdp-per-capita.csv'))

    csv = CSV.parse(csv_text, :headers => true)

    csv.each do |row| 

      if (row['Year'] == "2013" && row['Energy use per capita (kg oil equivalent) (kWh)'] != "nil")

 EnergyUsage.create!(:country => row['Entity'], :energy_use => row['Energy use per capita (kg oil equivalent) (kWh)'], :gdp_per_capita => row['GDP per capita (2011 int-$) (constant 2011 international $)'], :population => row['Total population (Gapminder)'])

end
end
     puts "#{Time.now} - Success!"


    end
  end






