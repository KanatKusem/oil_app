# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171130173955) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "code_data", id: false, force: :cascade do |t|
  end

  create_table "consumptions", force: :cascade do |t|
    t.text "Entity"
    t.text "Code"
    t.integer "Year"
    t.integer "Solar"
    t.integer "Other_renew"
    t.integer "Nuclear"
    t.integer "Hydropower"
    t.integer "Natural_gas"
    t.integer "Crude_oil"
    t.integer "Coal"
    t.integer "Traditional_bio"
    t.integer "Wind"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "energy_usages", force: :cascade do |t|
    t.string "country"
    t.integer "energy_use"
    t.integer "gdp_per_capita"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "population"
  end

  create_table "oil_reserves", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "oil_stats", force: :cascade do |t|
    t.text "Location"
    t.text "Indicator"
    t.text "Subject"
    t.text "Measure"
    t.text "Frequency"
    t.text "Flag_codes"
    t.integer "Time"
    t.integer "Value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "productions", force: :cascade do |t|
    t.string "country"
    t.integer "year"
    t.integer "quantity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "reserves", force: :cascade do |t|
    t.string "country"
    t.decimal "reservation"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.string "remember_digest"
    t.boolean "admin", default: false
    t.string "activation_digest"
    t.boolean "activated", default: false
    t.datetime "activated_at"
    t.string "reset_digest"
    t.datetime "reset_sent_at"
    t.index ["email"], name: "index_users_on_email", unique: true
  end

end
