class AddPopulationToEnergyUsage < ActiveRecord::Migration[5.1]
  def change
    add_column :energy_usages, :population, :integer
  end
end
