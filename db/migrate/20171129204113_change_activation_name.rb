class ChangeActivationName < ActiveRecord::Migration[5.1]
  def change
  	rename_column :users, :activation, :activation_digest
  end
end
