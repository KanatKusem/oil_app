class CreateEnergyUsages < ActiveRecord::Migration[5.1]
  def change
    create_table :energy_usages do |t|
      t.string :country
      t.integer :energy_use
      t.integer :gdp_per_capita

      t.timestamps
    end
  end
end
