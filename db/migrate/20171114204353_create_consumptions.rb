class CreateConsumptions < ActiveRecord::Migration[5.1]
  def change
    create_table :consumptions do |t|
        t.text "Entity"
        t.text "Code"
        t.integer "Year"
        t.integer "Solar"
        t.integer "Other_renew"
        t.integer "Nuclear"
        t.integer "Hydropower"
        t.integer "Natural_gas"
        t.integer "Crude_oil"
        t.integer "Coal"
        t.integer "Traditional_bio"
        t.integer "Wind"
        
      t.timestamps
    end
  end
end
