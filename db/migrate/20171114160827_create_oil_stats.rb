class CreateOilStats < ActiveRecord::Migration[5.1]
  def change
    create_table :oil_stats do |t|
  t.text "Location"
    t.text "Indicator"
    t.text "Subject"
    t.text "Measure"
    t.text "Frequency"
    t.text "Flag_codes"
    t.integer "Time"
    t.integer "Value"
      
      t.timestamps
    end
  end
end
