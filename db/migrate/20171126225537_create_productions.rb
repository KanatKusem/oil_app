class CreateProductions < ActiveRecord::Migration[5.1]
  def change
    create_table :productions do |t|
    	t.string :country
      	t.integer :year
      	t.integer :quantity

      t.timestamps
    end
  end
end
